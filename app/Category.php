<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;
class Category extends Model
{
    use UsesUuid;
    
    protected $guarded=[];
    public function pendaftaran(){
        return $this->hasMany(RaceRegistration::class);
    }
}
