<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;
class Order extends Model
{
    use UsesUuid;
    
    protected $guarded = [];
    public function race_registration(){
        return $this->belongsTo(RaceRegistration::class);
    }
}
