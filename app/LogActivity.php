<?php

namespace App;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

class LogActivity extends Model
{
    use UsesUuid;
    protected $fillable = [
        'subject', 'url', 'method', 'ip', 'agent', 'user_id'
    ];
    public function user(){
        return $this->belongsTo(User::class);
    }
}
