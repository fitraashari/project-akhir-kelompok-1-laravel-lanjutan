<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;
class Profile extends Model
{
    use UsesUuid;
    //
    protected $guarded=[];
    
    public function user(){
        return $this->belongsTo(User::class);
    }
}
