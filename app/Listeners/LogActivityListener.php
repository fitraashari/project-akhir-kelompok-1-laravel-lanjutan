<?php

namespace App\Listeners;

use App\Events\LogActivityEvent;
use App\LogActivity;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;

class LogActivityListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LogActivityEvent  $event
     * @return void
     */
    public function handle(LogActivityEvent $event)
    {
        
        $log = [];
    	$log['subject'] = $event->subject;
    	$log['url'] = request()->fullUrl();
    	$log['method'] = request()->method();
    	$log['ip'] = request()->ip();
    	$log['agent'] = request()->header('user-agent');
        $log['user_id'] = auth()->check() ? auth()->user()->id : 0;
        LogActivity::create($log);
    }
}
