<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;
class RaceRegistration extends Model
{
    use UsesUuid;
    //
    protected $guarded=[];
    protected $with = ['user','category'];
    public function user(){
        return $this->belongsTo(User::class)->with('profile');
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function order(){
        return $this->hasMany(Order::class);
    }
}
