<?php

namespace App;

use App\Traits\UsesUuid;
use App\Message;
use App\Chat;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, UsesUuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    // protected $with=['profile'];
    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    // chat baru
    public function chats()
    {
        return $this->hasMany(Chat::class);
    }

    protected $appends = [
        'avatar'
    ];
    public function getAvatarAttribute()
    {
        return 'https://www.gravatar.com/avatar/' . md5(strtolower($this->email));
    }
    public function messagesTo()
    {
        return $this->hasOne(Message::class, 'to_id')->latest();
    }
    public function messagesFrom()
    {
        return $this->hasOne(Message::class, 'from_id')->latest();
    }
    public function race_registration(){
        return $this->hasMany(RaceRegistration::class);
    }
    public function checkRole($role){
        if($this->role==$role){
            return true;
        }else{
            return false;
        }
    }
public function isAdmin()
   {
       if(auth()->user()->role=='admin'){
           return true;
       }else{
           return false;
       }
   } 

}
