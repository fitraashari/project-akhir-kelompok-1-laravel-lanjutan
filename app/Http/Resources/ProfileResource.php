<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    { 
        
        return [
            'response_code'=>'00',
            'response_status'=>'success',
            'data'=>[
            'nama_lengkap'=>$this['nama_lengkap'],
            'jenis_kelamin'=>$this['jenis_kelamin'],
            'tanggal_lahir'=>$this['tanggal_lahir'],
            'alamat'=>$this['alamat'],
            'no_telp'=>$this['no_telp'],
            'nama_kontak_darurat'=>$this['nama_kontak_darurat'],
            'no_telp_darurat'=>$this['no_telp_darurat'],
            ]
        ];
    }
}
