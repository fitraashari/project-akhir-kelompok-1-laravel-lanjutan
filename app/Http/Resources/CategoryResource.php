<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'response_code'=>'00',
            'response_status'=>'success',
            'data'=>[
                'nama'              => $this->nama,
                'deskripsi'         => $this->deskripsi,
                'tempat'            => $this->tempat,
                'waktu'             => $this->waktu,
                'biaya_pendaftaran' => $this->biaya_pendaftaran,
                'peserta' => $this->pendaftaran
            ]
        ];
    }
}
