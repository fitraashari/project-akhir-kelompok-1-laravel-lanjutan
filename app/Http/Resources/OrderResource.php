<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                'id'=>$this->id,
                'order_id'=>$this->order_id,
                'amount'=>$this->amount,
                'method'=>$this->method,
                'status'=>$this->status,
                'created_at'=>$this->created_at,
                'username'=>$this->race_registration->user->username,
                'category'=>$this->race_registration->category->nama
        ];
    }
}
