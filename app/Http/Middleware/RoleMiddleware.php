<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {   
        foreach ($roles as $role) {
            if(Auth::user()->checkRole($role)){
                return $next($request);
            }
            abort(403);
        }
    }
}
