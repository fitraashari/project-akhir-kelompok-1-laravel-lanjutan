<?php

namespace App\Http\Controllers\Lomba;

use App\Category;
use App\Events\LogActivityEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CategoryResource;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::orderBy('created_at','asc')->get();
        return new CategoryCollection($categories);
    }
    public function edit($id){
        return view('category.edit',['id'=>$id]);
    }
    public function show($id){
        $category = Category::where('id','=',$id)->with('pendaftaran')->first();
        return new CategoryResource($category);
    }
    public function store(CategoryRequest $request)
    {
        $category = Category::create($this->categoryStore());

        event(new LogActivityEvent(auth()->user()->username.' Membuat Kategori '.$request['nama']));

        return new CategoryResource($category);
    }

    public function update(CategoryRequest $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->update($this->categoryStore());

        event(new LogActivityEvent(auth()->user()->username.' Mengupdate Kategori '.$category->nama));

        return new CategoryResource($category);
    }

    public function delete($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        return response()->json([
            'response_code'=>'00',
            'response_status'=>'success',
        ]);
    }

    public function categoryStore()
    {
        return [
            'nama'              => request('nama'),
            'deskripsi'         => request('deskripsi'),
            'tempat'            => request('tempat'),
            'waktu'             => request('waktu'),
            'biaya_pendaftaran' => request('biaya_pendaftaran')
        ];
    }
    public function categoryPeserta($id){
        return view('category.peserta',['id'=>$id]);
    }
}
