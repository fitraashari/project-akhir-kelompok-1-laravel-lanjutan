<?php

namespace App\Http\Controllers\Lomba;

use App\Http\Controllers\Controller;
use App\Events\LogActivityEvent;
use App\Http\Requests\ProfileRequest;
use App\Http\Resources\ProfileResource;
use App\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    //
    public function index(){
        return view('profile.my');
    }
    public function currentUser(){
        $profile = Profile::where('user_id','=',auth()->user()->id)->first();
        return new ProfileResource($profile);
    }
    public function create(){
        return view('profile.create');
    }
    public function store(ProfileRequest $request){
        $profile = Profile::updateOrCreate([
            'user_id'=>auth()->user()->id,
        ],
        [
            'nama_lengkap'=>$request['nama_lengkap'],
            'jenis_kelamin'=>$request['jenis_kelamin'],
            'tanggal_lahir'=>$request['tanggal_lahir'],
            'alamat'=>$request['alamat'],
            'no_telp'=>$request['no_telp'],
            'nama_kontak_darurat'=>$request['nama_kontak_darurat'],
            'no_telp_darurat'=>$request['no_telp_darurat'],

        ]);
        event(new LogActivityEvent(auth()->user()->username.' Telah Mengupdate Profile'));
        return new ProfileResource($profile);
    }
}
