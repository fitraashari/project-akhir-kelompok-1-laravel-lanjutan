<?php

namespace App\Http\Controllers\Lomba;

use App\Events\LogActivityEvent;
use App\Http\Resources\LogActivityResource;
use App\Http\Controllers\Controller;
use App\LogActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LogActivityController extends Controller
{
    public function index(){
       
    }
    public function getAllData(){
        $log = LogActivity::orderBy('created_at','desc')->get();
        return new LogActivityResource($log);
    }
    public function destroy($id){
        $log = LogActivity::where('id',$id)->delete();
        // event(new LogActivityEvent(auth()->user()->username.' Menghapus Data Log Activity'));
        return response()->json([
            'response_code'=>'00',
            'response_status'=>'success'
        ]);
    }
}
