<?php

namespace App\Http\Controllers\Lomba;

use App\Events\LogActivityEvent;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\OrderResource;
use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function getAllData()
    {
        $orders = Order::with('race_registration')->get();
        // return $orders;
        return [
            'response_code'=>'00',
            'response_status'=>'success',
            'data'=>OrderResource::collection($orders)
        ];
    }
    public function index(){
        return view('order.index');
    }
    public function delete($id)
    {
        $order = Order::findOrFail($id)->first();
        event(new LogActivityEvent(auth()->user()->username.' Menghapus log Transaksi '.$order->order_id));
        $order->delete();
        return response()->json([
            'response_code'=>'00',
            'response_status'=>'success',
        ]);
    }
}
