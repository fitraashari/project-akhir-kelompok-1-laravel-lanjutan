<?php

namespace App\Http\Controllers\Lomba;

use App\Http\Controllers\Controller;
use App\Events\LogActivityEvent;
use App\Http\Requests\ProfileRequest;
use App\Http\Resources\ProfileResource;
use App\Http\Resources\UserCollection;
use App\Profile;
use App\User;
use Illuminate\Http\Request;

class PesertaController extends Controller
{
    //
    public function index(){
        return view('peserta.index');
    }
    public function getAllData(){
        $peserta = User::with('profile')->where('role','peserta')->has('profile')->get();
        
        return new UserCollection($peserta);
    }
    public function show($id){
        $peserta = Profile::where('id',$id)->get();
        
        return new ProfileResource($peserta);
    }
    public function destroy($id){
        $profile = Profile::where('user_id',$id)->first();
        $peserta = User::where('id',$id)->first();
        event(new LogActivityEvent(auth()->user()->username.' Telah Menghapus Peserta '.$peserta->username));
        $profile->delete();
        $peserta->delete();
        return response()->json([
            'response_code'=>'00',
            'response_status'=>'success',
        ]);
    }
    public function edit($id){
        
        return view('peserta.edit',['id'=>$id]);
    }
    public function update($id,ProfileRequest $request){
        $peserta = Profile::updateOrCreate([
            'user_id'=>$request['user_id'],
        ],
        [
            'nama_lengkap'=>$request['nama_lengkap'],
            'jenis_kelamin'=>$request['jenis_kelamin'],
            'tanggal_lahir'=>$request['tanggal_lahir'],
            'alamat'=>$request['alamat'],
            'no_telp'=>$request['no_telp'],
            'nama_kontak_darurat'=>$request['nama_kontak_darurat'],
            'no_telp_darurat'=>$request['no_telp_darurat'],
        ]);
        event(new LogActivityEvent(auth()->user()->username.' Telah Mengupdate Profile Peserta '.$request['nama_lengkap']));
        return new ProfileResource($peserta);
    }
}
