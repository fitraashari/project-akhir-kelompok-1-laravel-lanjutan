<?php

namespace App\Http\Controllers;

use App\Events\LogActivityEvent;
use App\Order;
use Illuminate\Http\Request;
use Midtrans;
class MidtransController extends Controller
{
    //
    public function generate(Request $request){
        // Set your Merchant Server Key
            Midtrans\Config::$serverKey = 'SB-Mid-server-FjBVu7oh9nyiddNsAyV_IAwa';
            // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
            Midtrans\Config::$isProduction = false;
            // Set sanitization on (default)
            Midtrans\Config::$isSanitized = true;
            // Set 3DS transaction for credit card to true
            Midtrans\Config::$is3ds = true;
            $midtrans_transaction =  Midtrans\Snap::createTransaction($request->data);
            // return $request->data['transaction_details']["order_id"];
            $order = Order::create([
                'race_registration_id'=>$request->race_registration_id,
                'order_id'=>$request->data['transaction_details']["order_id"],
                'amount'=>$request->data['transaction_details']["gross_amount"],
                'status'=>'pending',
                'method'=>'snap',
            ]);
            event(new LogActivityEvent(auth()->user()->username." melakukan pembayaran registrasi ".$request->race_registration_id));
            return response()->json([
                'response_code'=>'00',
                'response_status'=>'success',
                'data'=>$midtrans_transaction
            ]);
    }
}
