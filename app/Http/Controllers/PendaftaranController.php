<?php

namespace App\Http\Controllers;

use App\Category;
use App\Events\LogActivityEvent;
use App\Http\Resources\PendaftaranCollection;
use App\Http\Resources\PendaftaranResource;
use App\RaceRegistration;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

class PendaftaranController extends Controller
{
    //

    public function categoryList(){
        
        $user_register = RaceRegistration::where('user_id',auth()->user()->id)->pluck('category_id')->all();
        // $user_register=$user_register?$user_register->category_id:0;
        if($user_register){
            $kategori = Category::whereNotIn('id',$user_register)->get();
        }else{
            $kategori = Category::get();
        }
        return new PendaftaranCollection($kategori);
    }
    public function getAllData(){
        $pendaftaran = RaceRegistration::where('user_id',auth()->user()->id)->get();
        
        return new PendaftaranCollection($pendaftaran);
    }
    public function show($id){
        $pendaftaran = RaceRegistration::where('id','=',$id);
        
        return new PendaftaranResource($pendaftaran);
    }
    public function destroy($id){
        $pendaftaran = RaceRegistration::where('id','=',$id)->first();
        $pendaftaran->delete();
        event(new LogActivityEvent(auth()->user()->username." membatalkan pendaftaran lomba ".$pendaftaran->user->username." kategori ".$pendaftaran->category->nama));
        return response()->json([
            'response_code'=>'00',
            'response_status'=>'success',
        ]);
    }
    public function store(Request $request){
        request()->validate([
            'category_id'=>['required']
        ]);
        $pendaftaran = RaceRegistration::firstOrcreate([
            'user_id'=>auth()->user()->id,
            'category_id'=>request('category_id')
        ]);
        event(new LogActivityEvent(auth()->user()->username." mendaftar lomba"));
        return new PendaftaranResource($pendaftaran);
    }
    public function index(){
        return view('pendaftaran.create');
    }
    public function userRace(){
        return view('pendaftaran.list');
    }
    public function checkout($id){
        return view('pendaftaran.checkout',['id'=>$id]);
    }
    public function updatePembayaran($id){
        $pendaftaran = RaceRegistration::findOrFail($id)->first();
        if($pendaftaran->status_pembayaran=='success'){
            $status='pending';
        }else{
            $status='success';
        }
        $pendaftaran->update([
            'status_pembayaran'=>$status
        ]);
        event(new LogActivityEvent(auth()->user()->username." mengupdate status pembayaran ".$pendaftaran->user->username." kategori ".$pendaftaran->category->nama));
        return new PendaftaranResource($pendaftaran);
    }
}
