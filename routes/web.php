<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::middleware('auth')->group(function(){
    Route::view('/profile', 'profile.my');
    Route::get('/profile/user', 'Lomba\ProfileController@currentUser');
    Route::get('/profile/create', 'Lomba\ProfileController@create');
    Route::post('/profile/store', 'Lomba\ProfileController@store');
    Route::view('/profile/edit', 'profile.edit');
    Route::post('/profile/update', 'Lomba\ProfileController@store');

//middleware yang memaksa user untuk melengkapi profile mereka terlebih dahulu
Route::middleware('profile')->group(function(){
Route::get('/home', 'HomeController@index')->name('home');

// Chat Baru
Route::view('/chat', 'chat.index')->middleware('auth');

Route::get('/chat/all-chats', 'Lomba\ChatController@all_chats');

Route::post('/chat/store', 'Lomba\ChatController@store');

Route::get('/pendaftaran/list','PendaftaranController@getAllData');
Route::get('/pendaftaran/ku','PendaftaranController@userRace');
Route::get('/pendaftaran/{id}/bayar','PendaftaranController@checkout');
Route::get('/pendaftaran/{id}/show','PendaftaranController@show');
Route::post('/pendaftaran/{id}/cancel','PendaftaranController@destroy');

Route::get('/pendaftaran/kategori','PendaftaranController@categoryList');
Route::post('/pendaftaran/store','PendaftaranController@store');
Route::get('/pendaftaran','PendaftaranController@index');

Route::post('/generate','MidtransController@generate');


Route::middleware('role:admin')->group(function(){
    
Route::post('/pendaftaran/{id}/status','PendaftaranController@updatePembayaran');

    Route::get('/peserta', 'Lomba\PesertaController@index');
    Route::get('/peserta/all', 'Lomba\PesertaController@getAllData');
    Route::get('/peserta/{id}', 'Lomba\PesertaController@show');
    Route::get('/peserta/{id}/edit', 'Lomba\PesertaController@edit');
    Route::post('/peserta/{id}/update', 'Lomba\PesertaController@update');
    Route::post('/peserta/{id}/delete', 'Lomba\PesertaController@destroy');

     // Route order
Route::get('/orders', 'Lomba\OrderController@getAllData');
Route::post('/orders/{id}/delete/', 'Lomba\OrderController@delete');
Route::get('/log-trans','Lomba\OrderController@index');
    Route::view('/categories', 'category.index');
    Route::get('/categories/{id}/peserta', 'Lomba\CategoryController@categoryPeserta');
    Route::get('/categories/all', 'Lomba\CategoryController@index');
    Route::view('/categories/create', 'category.create');
    Route::get('/categories/{id}', 'Lomba\CategoryController@show');
    Route::get('/categories/{id}/edit', 'Lomba\CategoryController@edit');
    Route::post('/categories/store', 'Lomba\CategoryController@store');
    Route::post('/categories/{id}/update', 'Lomba\CategoryController@update');
    Route::post('/categories/{id}/delete', 'Lomba\CategoryController@delete');

    Route::view('/log', 'log');
    Route::get('/log/all', 'Lomba\LogActivityController@getAllData');
    Route::post('/log/{id}/delete', 'Lomba\LogActivityController@destroy');
});

});
});


// routing untuk message
// Route::group(['prefix' => 'message'], function () {
//     Route::get('user/{query}', 'MessageController@user');
//     Route::get('user-message/{id}', 'MessageController@message');
//     Route::get('user-message/{id}/read', 'MessageController@read');
//     Route::post('user-message', 'MessageController@send');
// });

