@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row ">
        <div class="col-lg-12">
                <script>
                    window.Laravel = {!! json_encode([
                        'id'=>$id
                    ]); !!}
                </script>
            <div id="app">
            <category-peserta-table/>

            </div>
        </div>
    </div>
</div>
@endsection
