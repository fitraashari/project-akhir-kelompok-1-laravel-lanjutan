@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row ">
        <div class="col-lg-12">
                <script>
                    window.Laravel = {!! json_encode([
                        'id'=>$id
                    ]); !!}
                </script>
            <div id="app">
            <checkout-pendaftaran/>
            </div>
        </div>
    </div>
</div>
<script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="SB-Mid-client-Pn52YOpKa5DnK-A7"></script>
@endsection
