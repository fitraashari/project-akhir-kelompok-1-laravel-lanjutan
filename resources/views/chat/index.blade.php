@extends('layouts.master')

@section('content')
<div id="app">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <chat-box-component/>
            </div>

            <div class="col-lg-4">
                <chat-user-list-component/>
            </div>
        </div>
    </div>
</div>
@endsection
