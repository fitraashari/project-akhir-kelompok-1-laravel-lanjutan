<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
      <i class="fa fa-bars"></i>
    </button>

    <!-- Topbar Search -->
   

    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="/profile">
          <i class="fas fa-user-alt fa-sm fa-fw mr-2 text-gray-400"></i>
          <span class="mr-2 d-none d-lg-inline text-gray-600 small">
          {{auth()->user()->username}}
          ({{auth()->user()->role}})
          </span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('logout') }}"
        onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                      
          <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
          <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                                      {{ __('Logout') }}
                      </span>

                                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                      @csrf
                                  </form>
        </a>
      </li>

    </ul>

  </nav>