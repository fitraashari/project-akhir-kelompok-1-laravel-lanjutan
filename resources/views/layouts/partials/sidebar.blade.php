<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
      <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-running"></i>
      </div>
      <div class="sidebar-brand-text mx-3">Kelompok <sup>1</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
      <a class="nav-link" href="/home">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    
    
      @if (auth()->user()->isAdmin())
      <hr class="sidebar-divider my-0">
      <li class="nav-item">
        <a class="nav-link" href="/peserta">
          <i class="fas fa-fw fa-users"></i>
          <span>Profile Peserta</span></a>
      </li>
      <hr class="sidebar-divider my-0">

    <li class="nav-item">
      <a class="nav-link" href="/categories">
        <i class="fas fa-fw fa-list"></i>
        <span>Category</span></a>
    </li>
    <hr class="sidebar-divider my-0">

    <li class="nav-item">
      <a class="nav-link" href="/log-trans">
        <i class="fas fa-fw fa-money-check-alt"></i>
        <span>Logs Transaction</span></a>
    </li>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
      <a class="nav-link" href="/log">
        <i class="fas fa-fw fa-folder"></i>
        <span>Logs Activity</span></a>
    </li>
    <hr class="sidebar-divider my-0">
                @else
                <hr class="sidebar-divider my-0">
                <li class="nav-item">
                  <a class="nav-link" href="/pendaftaran">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Pendaftaran</span></a>
                    <a class="nav-link" href="/pendaftaran/ku">
                      <i class="fas fa-fw fa-running"></i>
                      <span>Daftar Lomba Yang Diikuti</span>
                    </a>
                  </li>
                @endif
      

    <li class="nav-item">
      <a class="nav-link" href="/chat">
        <i class="fas fa-fw fa-comment"></i>
        <span>Chat</span></a>
      </li>
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

  </ul>