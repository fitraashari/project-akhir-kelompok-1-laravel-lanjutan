/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('category-list', require('./components/category/CategoryListComponent.vue').default);
Vue.component('category-form', require('./components/category/CategoryFormComponent.vue').default);
Vue.component('category-edit', require('./components/category/CategoryEditComponent.vue').default);
Vue.component('category-peserta-table', require('./components/category/CategoryPesertaComponent.vue').default);

Vue.component('profile-user', require('./components/profile/UserProfileComponent.vue').default);
Vue.component('profile-form', require('./components/profile/FormProfileComponent.vue').default);
Vue.component('profile-edit', require('./components/profile/EditProfileComponent.vue').default);

Vue.component('log-trans-table', require('./components/order/LogTransComponent.vue').default);
Vue.component('log-table', require('./components/log/LogComponent.vue').default);
Vue.component('create-pendaftaran', require('./components/pendaftaran/CreateComponent.vue').default);
Vue.component('list-pendaftaran', require('./components/pendaftaran/ListComponent.vue').default);
Vue.component('checkout-pendaftaran', require('./components/pendaftaran/CheckoutComponent.vue').default);
// Vue.component('detail-kategori-pendaftaran', require('./components/pendaftaran/DetailKategoriComponent.vue').default);
// Vue.component('bayar-pendaftaran', require('./components/pendaftaran/FormBayarComponent.vue').default);

Vue.component('peserta-table', require('./components/peserta/TableComponent.vue').default);
Vue.component('peserta-edit', require('./components/peserta/EditComponent.vue').default);

Vue.component('chat-box-component', require('./components/chat/ChatBoxComponent.vue').default);
Vue.component('chat-user-list-component', require('./components/chat/ChatUserListComponent.vue').default);
Vue.component('chat-message-component', require('./components/chat/ChatMessageComponent.vue').default);
Vue.component('chat-form-component', require('./components/chat/ChatFormComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 const app = new Vue({
     el: '#app',
 });