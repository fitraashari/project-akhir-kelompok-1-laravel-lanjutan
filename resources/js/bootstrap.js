window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) { }

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from 'laravel-echo';

window.Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    cluster: process.env.MIX_PUSHER_APP_CLUSTER,
    encrypted: true
});

require ('./echo');

// import Vue from 'vue'

// new Vue({
//     el: '#app',
//     data: {
//         id: document.querySelector('meta[name="user_id"]').content,
//         search: '',
//         messages: [],
//         users: [],
//         form: {
//             to_id: '',
//             content: ''
//         },
//         isActive: null,
//         notif: 0
//     },
//     mounted() {
//         this.fetchUsers()
//         this.fetchPusher()
//     },
//     methods: {
//         fetchUsers() {
//             let q = _.isEmpty(this.search) ? 'all' : this.search

//             axios.get('/message/user/' + q).then(({ data }) => {
//                 this.users = data
//             })
//         },
//         fetchMessages(id) {
//             this.form.to_id = id
//             axios.get('/message/user-message/' + id).then(({ data }) => {
//                 this.messages = data
//                 this.isActive = this.users.findIndex((s) => s.id === id)
//                 this.users[this.isActive].count = 0
//                 this.notif--
//             })
//         },
//         sendMessage() {
//             axios.post('message/user-message', this.form).then(({ data }) => {
//                 this.pushMessage(data, data.to_id)
//                 this.form.content = ''
//                 this.search = ''
//             })
//         },
//         fetchPusher() {
//             Echo.channel('user-message.' + this.id)
//                 .listen('MessageEvent', (e) => {
//                     this.pushMessage(e, e.from_id, 'push')
//                 })
//         },
//         pushMessage(data, user_id, action = '') {
//             let index = this.users.findIndex((s) => s.id === user_id)

//             if (index != -1 && action == 'push') {
//                 this.users.splice(index, 1)
//             }

//             /**
//              * if untuk pesan submit
//              */
//             if (action == '') {
//                 this.users[index].content = data.content
//                 this.users[index].to_id = data.to_id

//                 let user = this.users[index]

//                 this.users.splice(index, 1)
//                 this.users.unshift(user)
//             }

//             /**
//              * else untuk pesan dari laravel echo
//              */
//             else {
//                 this.users.unshift(data)
//             }

//             /**
//              * Jika dia melihat pesan user
//              */
//             if (this.form.to_id != '') {
//                 index = this.users.findIndex((s) => s.id === this.form.to_id)

//                 this.users[index].count = 0
//                 this.isActive = index

//                 if (this.form.to_id == user_id) {

//                     this.messages.push({
//                         avatar: data.avatar,
//                         content: data.content,
//                         created_at: data.created_at,
//                         from_id: data.from_id,
//                     })

//                     axios.get('/message/user-message/' + user_id + '/read')

//                 }

//             }
//         },
//         scrollToEnd: function () {
//             let container = this.$el.querySelector("#card-message-scroll");
//             container.scrollTop = container.scrollHeight;
//         }
//     },
//     watch: {
//         search: _.debounce(function () {
//             this.fetchUsers()
//         }, 500),
//         users: _.debounce(function () {
//             this.notif = 0
//             this.users.filter(e => {
//                 if (e.count) {
//                     this.notif++
//                 }
//             })
//         }),
//         messages: _.debounce(function () {
//             this.scrollToEnd()
//         }, 10),
//     }
// })
