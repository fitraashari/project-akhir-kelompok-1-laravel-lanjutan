<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'nama' => $faker->sentence(1),
        'deskripsi' => $faker->paragraph,
        'tempat' => $faker->sentence(2),
        'waktu' => now(),
        'biaya_pendaftaran'=>120000,
    ];
});
